<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow-x : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	
	function getGroup(){
		var url = "${ctxPath}/getJigList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log(data);
				json = data.jigList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decodeURIComponent(data.jig) + "'>" + decodeURIComponent(data.jig) + "</option>"; 
				});
				
				$("#group").html(option);
				
				getDvcList();
			}
		});
	};
	

	var tmpInCycleBar, tmpCuttingBar, tmpWaitBar, tmpAlarmBar, tmpNoConnBar, tmpArray;
	var excelData, excelData2;
	
	function getDvcList(){
		var url = "${ctxPath}/getTableData.do";

		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + " 23:59:59" + 
					"&shopId=" + shopId +
					"&jig=" + $("#group").val();
				
		if($("#sDate").val() > $("#eDate").val()){
			alert("시작 날짜가 더 최근일 수 없습니다.");
			return;
		}
		
		window.localStorage.setItem("jig_sDate", sDate);
		window.localStorage.setItem("jig_eDate", eDate);
		
	console.log(param)		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.tableData;
				console.log(json);
				//console.log(json[0].name);
				
				var start = new Date(sDate);
				var end = new Date(eDate);
				var n = (end - start)/(24 * 3600 * 1000)+1;
				
				var tr = "";

				wcList = new Array();
				var wc = new Array();
				
								
				/* wc.push("${division}");
				wc.push("${ophour}");
				wc.push("${wait}");
				wc.push("${stop}");
				wc.push("${noconnection}"); */
				
								
				wc.push("${division}");
				wc.push("${cut}");
				wc.push("${incycle}");
				wc.push("${wait}");
				wc.push("${stop}");
				wc.push("${off}");
				
				wcList.push(wc);

				inCycleBar = [];
				cuttingBar = [];
				waitBar = [];
				alarmBar = [];
				noConnBar = [];
				wcName = [];
				tmpArray = [];
				cBarPoint = 0;
				cPage = 1;
				
				tmpInCycleBar = 0;
				tmpCuttingBar = 0;
				tmpWaitBar = 0;
				tmpAlarmBar = 0;
				tmpNoConnBar = 0;
				tmpWcList = [];
								
				
				$(json).each(function(idx, data){
					wcName.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
						 
					var wc = new Array();
					wc.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
					//wc.push("M" + decodeURIComponent(idx).replace(/\+/gi, " "));
					wc.push(data.cutting_time);
					wc.push(data.inCycle_time);
					wc.push(data.wait_time);
					wc.push(data.alarm_time);
					wc.push(data.noConnTime);
					wc.push(data.WC);
					
					wcList.push(wc);
					tmpWcList = wcList;
					
					var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
					var cutting = Number(Number(data.cutting_time/60/60).toFixed(1));
					var wait = Number(Number(data.wait_time/60/60).toFixed(1));
					var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
					/* var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1); */
					var noconn = Number(Number(data.noConnTime/60/60).toFixed(1));
					
					cuttingBar.push(cutting);
					inCycleBar.push(incycle);					
					waitBar.push(wait);
					alarmBar.push(alarm);
					noConnBar.push(noconn);
					
					tmpInCycleBar = inCycleBar;
					tmpCuttingBar = cuttingBar;
					tmpWaitBar = waitBar;
					tmpAlarmBar = alarmBar;
					tmpNoConnBar = noConnBar;
				});
			
				var blank = maxBar - json.length
				maxPage = json.length - maxBar;
				jsonLen = json.length;
				
				for(var i = 0; i < maxBar - json.length % 10; i++){
					wcName.push("");
					var wc = new Array();
					wc.push("______");
					wc.push("");
					wc.push("");
					wc.push("");
					wc.push("");
					wc.push("");
					
					wcList.push(wc);
					
					inCycleBar.push(0);
					cuttingBar.push(0);
					waitBar.push(0);
					alarmBar.push(0);
					noConnBar.push(0);
				};
				
				tmpArray = wcName;
				
				if(json.length > maxBar){
					$("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
				}
				
				var stDate = new Date($("#sDate").val()) ;
			    var endDate = new Date($("#eDate").val()) ;
			 
			    var btMs = endDate.getTime() - stDate.getTime() ;
			    var btDay = btMs / (1000*60*60*24) ;
								    
				var day_cnt = btDay+1;
				
				
				// 엑셀 저장 excel
				excelData = "";
				excelData2 = "LINE" + "평균" + "LINE" + "LINE";
				
				excelWcList = [];
				excelWcList = wcList;
				
				// console.log("excelWcList");
				// console.log(excelWcList);
							
				var excelLength = excelWcList.length;
				
				for(var i=0; i<excelWcList.length; i++){
					
					if(excelWcList[i][0].indexOf('______') == 0){
						excelLength = i;
						console.log("______ 의 최초 위치 : " + excelLength);
						break;
					}	
				}
				
				for(var i = 0; i < excelWcList[0].length; i++){	
						
					for(var j = 0; j < excelLength; j++){
						
						if(j==0){//매 줄 첫 번 째 td
									
							excelData += excelWcList[j][i] + ",";
							excelData2 += excelWcList[j][i] + ",";
							
						}else if(j==0 || i==0){//장비 이름
							
							if(j < excelLength -1 ){
								excelData += excelWcList[j][i] + ",";
								excelData2 += excelWcList[j][i] + ",";
							} else{
								excelData += excelWcList[j][i] + "LINE";
								excelData2 += excelWcList[j][i] + "LINE";
							}	
							
						}else {
							var n;
							if(typeof(excelWcList[j][i])=="number"){
								n = Number(excelWcList[j][i]/60/60).toFixed(1);
							}else{
								n = "";
							};
							
								if(j < excelLength -1 ){
									excelData += n + ",";
									excelData2 += Number(n/day_cnt).toFixed(1) + ",";
								}else{
									excelData += n + "LINE";
									excelData2 += Number(n/day_cnt).toFixed(1) + "LINE";
								}
						};
					};
				};
				
				//console.log(excelData2);
				
				excelData += excelData2;
				
				reArrangeArray(jsonLen);
				
				$(".chartTable").remove();
				var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
				var table_total = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
				
				table_total += "<tr class='contentTr' >" + 
									"<td class='content' align='center' colspan='19' style='font-weight: bolder; background-color: rgb(34,34,34);'>${avrg}</td>" +
								"</tr>";
				
				
				for(var i = 0; i < wcList[0].length; i++){
					table += "<tr class='contentTr'>";
					table_total += "<tr class='contentTr'>";
	
					var bgColor;
					
					if(i==1){
						bgColor = cuttingColor;
						color = "#A6A6A6";
					}else if(i==2){
						bgColor = incycleColor;
						color = "black";
					}else if(i==3){
						bgColor = waitColor;
						color = "black";
					}else if(i==4){
						bgColor = alarmColor;
						color = "black";
					}else if(i==5){
						bgColor = noconnColor;
						color = "black";
					}else{
						color = "white";
						bgColor = "#323232";
					}
										
					for(var j = 0; j < wcList.length; j++){
						if(j==0){//매 줄 첫 번 째 td
							table += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + " ; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
							
							if(i!=0) table_total += "<td style='font-weight: bolder; background-color: " + bgColor + "; color : " + color + "; width:" + getElSize(300) + "; ' class='title_'>" + wcList[j][i] + "</td>";
							
						}else if(j==0 || i==0){//장비 이름
							table += "<td style='font-weight: bolder; background-color: " + bgColor + ";' class='content' onclick='toDailyChart(\"" + wcList[j][i] + "\"," + "\"" + wcList[j][i+5] + "\")'>" + wcList[j][i] + "</td>";
							
						}else {
							var n;
							if(typeof(wcList[j][i])=="number"){
								n = Number(wcList[j][i]/60/60).toFixed(1);
							}else{
								n = "";
							};
							
							table += "<td class='content'>" + n + "</td>";
							
							if(wcList[j][0] == '______'){
								table_total += "<td class='content'> </td>";
							} else{
								table_total += "<td class='content'>" + Number(n/day_cnt).toFixed(1) + "</td>";
							}
						};
					};
					table += "</tr>";
					table_total += "</tr>";
				};
				
				table += "</table>";
				table_total += "</table>";
				$("#tableContainer").append(table)
			
				$("#tableContainer_avg").append(table_total)
						
				//setEl();
				
				$(".title_").css({
					"width" : getElSize(300),
					"padding" : getElSize(5),
					"font-size" : getElSize(40)
				});
				
				//console.log($(".contentTr").width())
				$(".content").css({
					"font-size" : getElSize(40),
					"width" : ($(".contentTr").width() - getElSize(300))/10
				})
				
				chart("chart");
// 				addSeries();
				
				
			//	$("#tableContainer").css("margin-top",$("#chart").offset().top + $("#chart").height() - $(".mainTable").height() + getElSize(50))
			}
		});
	};
	
	function csvSend(){
		var id = this.id;
		var sDate, eDate;
		var csvOutput;
		
		sDate = $("#sDate").val();
		eDate = $("#sDate").val();
		csvOutput = excelData;
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit();
	};

	var barChart;
	var maxBar = 10;
	var jsonLen;
	
	function chart(id){
		
		var margin = (originWidth*0.05)/2;
		$("#" + id).css({
			//"margin-left" : $("#tableContainer table td:nth(1)").offset().left - margin- getElSize(150) - $(".menu_left").width(),
			//"width" : $("#tableContainer table").width() - $("#tableContainer table td:nth(0)").width(),
			"margin-left" : getElSize(80),
			"width" : getElSize(3200),
			//"top" : $(".label").offset().top + $(".label").height() + getElSize(50)
		})
		
		$('#' + id).highcharts({
	        chart: {
	            type: 'column',
	            backgroundColor : 'rgb(50, 50, 50)',
	            height :getElSize(900), //950
	           	marginLeft:getElSize(150),
	            marginRight:0,
	            marginBottom : getElSize(250)
	        },
	        title: {
	            text:false
	        },
	        xAxis: {
	            categories: wcName,
	            labels : {
	            	style : {
	            		"color" : "white",
	            		"font-size" : getElSize(40)
	            	}
	            }
	        },
	        yAxis: {
	            min: 0,
	            //max : 20,
	            title: {
	                text: "Hour (h)",
	                style:{
	                	color : "white"
	                }
	            },
	            labels : {
	            	style : {
	            		color : "white",
	            		"font-size" : getElSize(30)
	            	},
	            	enabled : true
	            },
	            //reversed:true 
	        },
	        tooltip: {
	            formatter: function () {
	                return this.y;
	            }
	        },
	        plotOptions: {
	            column: {
	                stacking: 'normal',
	                //minPointLength: 0,
	                dataLabels: {
	                	allowOverlap: true,
		                formatter: function () {
	                		return (this.y>0)?this.y:"";
    	            	},
	                    enabled: true,
	                    //backgroundColor: 'rgba(0,0,0,0.3)',
	                    //crop: false,
	                    //overflow: 'none',
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
	                    style: {
	                    	fontSize : getElSize(40)
	                        //textShadow: '0 0 3px black',
	                    }
	                }
	            },
	            series:{
	            	//groupPadding: 0.125,
	            	pointWidth: getElSize(120)
	            }
	        },
	        credits : false,
	        exporting : false,
	        legend:{
	        	enabled : false
	        },
	        series: [{
	        	color : cuttingColor,
	        	data : cuttingBar	
	        },{
	        	color : incycleColor,
	        	data : inCycleBar
	        },{
	        	color :waitColor,
				data : waitBar
	        },{
	        	color : alarmColor,
				data : alarmBar
	        },{
	        	color : "gray",
				data : noConnBar
	        }]
	    });
		
		barChart = $("#" + id).highcharts();
				
		for(var i=0; i < barChart.series.length; i++){
			
			var points = barChart.series[i].data;
			
			//console.log(points[0].shapeArgs.height);

	        for (var j = 0; j < points.length; j++) {

	            if (points[j].shapeArgs.height < 20) {
	                
	                var y = points[j].dataLabel.y,
	                    x = points[j].dataLabel.x;
	                
	                
	             	
	                if(i % 2 == 0){
	                	x += getElSize(106);
	                	console.log(i);
	                }else{
	                	x -= getElSize(106);
	                }
	                
	                               
	                points[j].dataLabel
	                .css({
	                    color: 'white'
	                });
	                
	                points[j].dataLabel.attr({
	                	y : y,
	                	x : x
	               	});
	        	};
	        };
	        
//	         console.log(barChart.series[1].data[0].dataLabel.x);
// 	         console.log(barChart.series[1].data[0]);
		}
		
	}
	
	/* function addSeries(){
		console.log(2)
		barChart.addSeries({
			color : cuttingColor,
			data : cuttingBar
		}, true);
		
		barChart.addSeries({
			color : incycleColor,
			data : inCycleBar
		}, true);
		
		barChart.addSeries({
			color :waitColor,
			data : waitBar
		}, true);
		
		barChart.addSeries({
			color : alarmColor,
			data : alarmBar
		}, true);
		
		barChart.addSeries({
			color : "gray",
			data : noConnBar
		}, true);
	}; */

	var wcDataList = new Array();
	var wcList = new Array();
	var wcName = new Array();
	var maxPage;
	var cBarPoint = 0;

	
	function reArrangeArray(length){
		
		inCycleBar = new Array();
		cuttingBar = new Array();
		waitBar = new Array();
		alarmBar = new Array();
		noConnBar = new Array();
		wcName = new Array();
		wcList = new Array();
		
		for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
			inCycleBar[i-cBarPoint] = tmpInCycleBar[i];
			cuttingBar[i-cBarPoint] = tmpCuttingBar[i];
			waitBar[i-cBarPoint] = tmpWaitBar[i];
			alarmBar[i-cBarPoint] = tmpAlarmBar[i];
			noConnBar[i-cBarPoint] = tmpNoConnBar[i];
			wcName[i-cBarPoint] = tmpArray[i];
		};
		
		if(tmpWcList.length == 0){
			
			var wc = new Array();
			
			wc.push("${division}");
			wc.push("${cut}");
			wc.push("${incycle}");
			wc.push("${wait}");
			wc.push("${stop}");
			wc.push("${off}");
			
			//wcList.push(wc);
			
			wcList[0] = wc;
						
			for(var i = 0; i < maxBar - length % 10; i++){
				wcName.push("");
				
				wc = [];
				wc.push("______");
				wc.push("");
				wc.push("");
				wc.push("");
				wc.push("");
				wc.push("");
				
				wcList.push(wc);
			};
			
		} else{
			for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
				wcList[i-cBarPoint] = tmpWcList[i];	
								
				var wc = new Array();
				
				wc.push("${division}");
				wc.push("${cut}");
				wc.push("${incycle}");
				wc.push("${wait}");
				wc.push("${stop}");
				wc.push("${off}");
												
				wcList[0] = wc;
			};		
		}
		
		console.log("wcList");
		console.log(wcList);
		
		
		$("#controller font").html(addZero(String(cPage)) + " / " + addZero(String(Math.ceil((maxPage + maxBar) / 10))));
	};

	var cPage = 1;
	
	function nextBarArray(){
		if(cBarPoint>=maxPage || maxPage<=0){
			alert("${end_of_chart}");
			return;
		};
		cBarPoint += maxBar;
		cPage++;
		
		reArrangeArray(jsonLen);
		
		$(".chartTable").remove();
		var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		var table_total = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		
		table_total += "<tr class='contentTr' >" + 
							"<td class='content' align='center' colspan='19' style='font-weight: bolder; background-color: rgb(34,34,34);'>${avrg}</td>" + 
						"</tr>";
		
	 	var stDate = new Date($("#sDate").val()) ;
	    var endDate = new Date($("#eDate").val()) ;
	 
	    var btMs = endDate.getTime() - stDate.getTime() ;
	    var btDay = btMs / (1000*60*60*24) ;
						    
		var day_cnt = btDay+1;

		for(var i = 0; i < wcList[0].length; i++){
			table += "<tr class='contentTr'>";
			table_total += "<tr class='contentTr'>";
			
			var bgColor;
			if(i==1){
				bgColor = cuttingColor;
				color = "#A6A6A6";
			}else if(i==2){
				bgColor = incycleColor;
				color = "black";
			}else if(i==3){
				bgColor = waitColor;
				color = "black";
			}else if(i==4){
				bgColor = alarmColor;
				color = "black";
			}else if(i==5){
				bgColor = noconnColor;
				color = "black";
			}else{
				color = "white";
				bgColor = "#323232";
			}
			
			for(var j = 0; j < wcList.length; j++){
				if(j==0){
					table += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
					
					if(i!=0) table_total += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
				}else if(j==0 || i==0){
					table += "<td class='content_' style='font-weight: bolder; background-color: " + bgColor + "; width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\"," + "\"" + wcList[j][i+5] + "\")'>" + wcList[j][i] + "</td>";
				}else {
					var n;
					if(typeof(wcList[j][i])=="number"){
						n = Number(wcList[j][i]/60/60).toFixed(1)
					}else{
						n = "";
					};
					
					table += "<td class='content_'>" + n + "</td>";
					
					if(wcList[j][0] == '______'){
						table_total += "<td class='content'> </td>";
					} else{
						// table_total += "<td class='content'>" + Number(n/day_cnt).toFixed(1) + "</td>";
						table_total += "<td class='content_' style='width:" + $("#chart").width()/(maxBar-1) + "'>" + Number(n/day_cnt).toFixed(1) + "</td>";
					}
				};
			};
			table += "</tr>";
			table_total += "</tr>";
		};
		
		table += "</table>";
		table_total += "</table>";
		$("#tableContainer").append(table)

		$("#tableContainer_avg").append(table_total)
		
		//setEl();
		chart("chart");
 		//addSeries();
		
		$(".title_").css({
			"width" : getElSize(300),
			"padding" : getElSize(5),
			"font-size" : getElSize(40)
		});
		
		$(".content_, .content").css({
			"font-size" : getElSize(40),
			"width" : ($(".contentTr").width() - getElSize(300))/10
		})
		
		$("#arrow_left").attr("src", ctxPath + "/images/left_en.png");
		if(Math.ceil((maxPage + maxBar) / 10)==cPage) $("#arrow_right").attr("src", ctxPath + "/images/right_dis.png");
	};

	function prevBarArray(){
		if(cBarPoint<=0){
			alert("${first_of_chart}");
			return;
		};
		cBarPoint -= maxBar;
		cPage--;
		reArrangeArray(jsonLen);
		
		$(".chartTable").remove();
		var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		var table_total = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		
		table_total += "<tr class='contentTr' >" + 
							"<td class='content' align='center' colspan='19' style='font-weight: bolder; background-color: rgb(34,34,34);'>${avrg}</td>" + 
						"</tr>";
		
	 	var stDate = new Date($("#sDate").val()) ;
	    var endDate = new Date($("#eDate").val()) ;
	 
	    var btMs = endDate.getTime() - stDate.getTime() ;
	    var btDay = btMs / (1000*60*60*24) ;
						    
		var day_cnt = btDay+1;

		for(var i = 0; i < wcList[0].length; i++){
			table += "<tr class='contentTr'>";
			table_total += "<tr class='contentTr'>";
			
			var bgColor;
			if(i==1){
				bgColor = cuttingColor;
				color = "#A6A6A6";
			}else if(i==2){
				bgColor = incycleColor;
				color = "black";
			}else if(i==3){
				bgColor = waitColor;
				color = "black";
			}else if(i==4){
				bgColor = alarmColor;
				color = "black";
			}else if(i==5){
				bgColor = noconnColor;
				color = "black";
			}else{
				color = "white";
				bgColor = "#323232";
			}
			
			for(var j = 0; j < wcList.length; j++){
				if(j==0){
					table += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
					
					if(i!=0) table_total += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
				}else if(j==0 || i==0){
					table += "<td class='content_' style='font-weight: bolder; background-color: " + bgColor + "; width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\"," + "\"" + wcList[j][i+5] + "\")'>" + wcList[j][i] + "</td>";
				}else {
					var n;
					if(typeof(wcList[j][i])=="number"){
						n = Number(wcList[j][i]/60/60).toFixed(1)
					}else{
						n = "";
					};
					
					table += "<td class='content'>" + n + "</td>";
					
					if(wcList[j][0] == '______'){
						table_total += "<td class='content'> </td>";
					} else{
						// table_total += "<td class='content'>" + Number(n/day_cnt).toFixed(1) + "</td>";
						table_total += "<td class='content' style='width:" + $("#chart").width()/(maxBar-1) + "'>" + Number(n/day_cnt).toFixed(1) + "</td>";	
					}
				};
			};
			table += "</tr>";
			table_total += "</tr>";
		};
		
		table += "</table>";
		table_total += "</table>";
		$("#tableContainer").append(table)

		$("#tableContainer_avg").append(table_total)
		
		//setEl();
		chart("chart");
		//addSeries();
		
		$(".title_").css({
			"width" : getElSize(300),
			"padding" : getElSize(5),
			"font-size" : getElSize(40)
		});
		
		$(".content_, .content").css({
			"font-size" : getElSize(40),
			"width" : ($(".contentTr").width() - getElSize(300))/10
		})
		
		$("#arrow_right").attr("src", ctxPath + "/images/right_en.png");
		if(cBarPoint==0) $("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$("#sDate").val(caldate(7));
		$("#eDate").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 10 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
	}
	
	var handle = 0;
	
	var incycleColor = "#50BA29";
	var cuttingColor = "#175501";
	var waitColor = "yellow";
	var alarmColor = "red";
	var noconnColor= "#A0A0A0";
	
	function upDateS(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		//changeDateVal();
	};
	
	function downDateS(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		//changeDateVal();
	};
	
	function upDateE(){
		var $date = $("#eDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#eDate").val(today);
		//changeDateVal();
	};
	
	function downDateE(){
		var $date = $("#eDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#eDate").val(today);
		//changeDateVal();
	};
	
	
	$(function(){
		$("#upS").click(upDateS);
		$("#downS").click(downDateS);
		$("#upE").click(upDateE);
		$("#downE").click(downDateE);
		
		createNav("analysis_nav",0);
		getGroup();
		setDate();	
		time();
		setEl();
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			//"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		/* $("#excel").css({
			"background" : "rgb(250,235,255)",
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		}); */
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		if(getParameterByName('lang')=='ko'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 9997,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 9997,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 9997,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		};
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 3,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$("select, button, input").not(".dateController").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").not(".dateController").css({
			"padding" : getElSize(15),
		})
		
		$(".dateController").css({
			"width" : getElSize(70) *  1.2,
			"height" : getElSize(70),
		});
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
			"position" : "relative",
			"top" : getElSize(26)
		});
		
		$("#chart").css({
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20),
			"margin-top" : getElSize(20),
		});
		
		$("#arrow_left").css({
			"width" : getElSize(60),
			"margin-right" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#arrow_right").css({
			"width" : getElSize(60),
			"margin-left" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#controller").css({
			"display" : "table",
			"background-color" : "black",
			"border-radius" : getElSize(70),
			"position" : "absolute",
			"bottom" : getElSize(845),
			"padding" : getElSize(15),
			//"width" : getElSize(600),
			"vertical-align" : "middle"
		});
		
		$("#controller").css({
			"left" : $("#content_table").offset().left - marginWidth + ($("#content_table").width()/2) - ($("#controller").width()/2)
		});
		
		$("img").css({
			"display" : "inline"
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(100),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#upE").css({
			"margin-left" : getElSize(19)
		});
		
		$("#menuTab").css({
		    "position": "absolute",
		    "top": getElSize(140),
		    "color": "white"
		})
	};
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		//location.href = "${ctxPath}/performanceReport.do"
		location.href = "${ctxPath}/performanceReport_table.do"
	};
	var color = "";
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
		<input type="hidden" name="title" value="Performance_Report_Chart">
	</form>
	
	<div id="time"></div>
	<div id="title"><spring:message code="operation_chart"></spring:message></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none" >
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/analysis_left.png" class='menu_left' style="display: none"  >
				</td>
				<td height="100%">
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right' style="display: none" >
					<div id="menuTab">
						&nbsp;&nbsp;
						<spring:message code="device_group"></spring:message>
						<select id="group"></select>
						<spring:message code="op_period"></spring:message>
						<button id="upS" class='dateController'>▲︎</button><button id="downS" class='dateController'>▼</button> 
						<input type="date" class="date" id="sDate"> ~ <button id="upE" class='dateController'>▲︎</button><button id="downE" class='dateController'>▼</button>
						<input type="date" class="date" id="eDate"> 
						<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getDvcList()">
						<button onclick="goGraph()" id="graph"><spring:message code="table"></spring:message> </button>
						<button onclick="csvSend()" id="excel"> EXCEL </button>

					</div>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'><spring:message code="performance_chart"></spring:message></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left nav' style="display: none" >
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="chart" style="width: 100%">
								</div>
								
								<div id="tableContainer" width="100%">
								</div>
								
								<div id="tableContainer_avg" width="100%" >
								</div>
								
								<div id="controller">
									<img alt="" src="${ctxPath }/images/left_en.png" id="arrow_left"  onclick="prevBarArray();">
									<font style="display: table-cell; vertical-align:middle">01 / 00</font>
									<img alt="" src="${ctxPath }/images/right_en.png" id="arrow_right" onclick="nextBarArray();">
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>	
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/selected_green.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none"  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none"  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none"  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'style="display: none"   >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>