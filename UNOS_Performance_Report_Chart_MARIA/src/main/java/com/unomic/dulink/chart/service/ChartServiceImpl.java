package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
 

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService{
	private final static String namespace= "com.unomic.dulink.chart.";
	
	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}
	
	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo),"utf-8");
		return str;
	}
	
	
	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		
		int exist = (int) sql.selectOne(namespace + "login", chartVo);
		
		if(exist==0){
			str = "fail";
		}else{
			str = "success";
		};
		
		return str;
	};
	
	//common func
	

	
	@Override 
	public String getJicList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> jigList  = sql.selectList(namespace + "getJicList", chartVo);
		logger.info ("getJicList: " + jigList);
		
		List jigs = new ArrayList();
		for(int i = 0; i < jigList.size(); i++){
			Map map = new HashMap();
			map.put("jig",  URLEncoder.encode(jigList.get(i).getJig(),"utf-8"));
			
			jigs.add(map);
			
			logger.info ("map = " + map);
			logger.info ("jigs = " + jigs);			
		};
		logger.info ("this@@");
		Map jigMap = new HashMap();
		jigMap.put("jigList", jigs);  // dataList => jigList
		
		String str = "";
		ObjectMapper ob = new ObjectMapper();
		str = ob.defaultPrettyPrintingWriter().writeValueAsString(jigMap);
		logger.info ("jigMap = " + jigMap);
		logger.info ("str = " + str);
		return str;
	};
	
	@Override
	public String getTableData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> dataList = sql.selectList(namespace + "getDataList", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			map.put("jig", URLEncoder.encode(dataList.get(i).getJig(), "utf-8"));
//			map.put("WC", URLEncoder.encode(dataList.get(i).getWC(), "utf-8"));
//			map.put("WCG", URLEncoder.encode(dataList.get(i).getGRNM(), "utf-8"));
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("target_time", dataList.get(i).getTargetDateTime());
			map.put("inCycle_time", dataList.get(i).getInCycleTime());
			map.put("cutting_time", dataList.get(i).getCuttingTime());
			map.put("wait_time", dataList.get(i).getWaitTime());
			map.put("alarm_time", dataList.get(i).getAlarmTime());
			map.put("noConnTime", dataList.get(i).getNoConnectionTime());
			map.put("line", dataList.get(i).getLine());
			
			list.add(map);
		};
		
		Map dataMap = new HashMap();
		dataMap.put("tableData", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
};