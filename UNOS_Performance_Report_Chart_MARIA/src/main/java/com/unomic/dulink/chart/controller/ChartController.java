package com.unomic.dulink.chart.controller;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.service.ChartService;
/**
 * Handles requests for the application home page.
 */
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/")
@Controller
public class ChartController {

	private static final Logger logger = LoggerFactory.getLogger(ChartController.class);
	final int shopId = 1;
	/**    
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private ChartService chartService; 
	     
	         
	@RequestMapping(value="index")
	public String alarmReport(){
		return "chart/index";
	};
	      
	@RequestMapping(value="index2")
	public String alarmReport2(){
		return "chart/index2";
	};
	
	@RequestMapping(value="performanceReport_table")
	public String performanceReport_table(){
		return "chart/performanceReport_table";
	};
	
	
	@RequestMapping(value="getTableData")
	@ResponseBody
	public String getTableData(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getTableData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getJigList")
	@ResponseBody
	public String getJicList(ChartVo chartVo){
		String str = null;
		try {
			str = chartService.getJicList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	
	
	//common func
	
	@RequestMapping(value="login")
	@ResponseBody 
	public String loginCnt(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.login(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="getStartTime")
	@ResponseBody
	public String getStartTime(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getStartTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	
	@RequestMapping(value="getComName")
	@ResponseBody 
	public String getComName(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getComName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getBanner")
	@ResponseBody
	public ChartVo getBanner(ChartVo chartVo){
		try {
			chartVo = chartService.getBanner(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return chartVo;
	}
};

